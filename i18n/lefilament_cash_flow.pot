# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* lefilament_cash_flow
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-01-17 12:18+0000\n"
"PO-Revision-Date: 2018-01-17 12:18+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: lefilament_cash_flow
#. openerp-web
#: code:addons/lefilament_cash_flow/static/src/xml/cash_flow.xml:12
#, python-format
msgid "Advanced cash"
msgstr ""

#. module: lefilament_cash_flow
#. openerp-web
#: code:addons/lefilament_cash_flow/static/src/xml/cash_flow.xml:9
#, python-format
msgid "Current balance"
msgstr ""

#. module: lefilament_cash_flow
#. openerp-web
#: code:addons/lefilament_cash_flow/static/src/xml/cash_flow.xml:11
#, python-format
msgid "Delay 1st facture"
msgstr ""

#. module: lefilament_cash_flow
#: model:ir.model,name:lefilament_cash_flow.model_project_project
msgid "Project"
msgstr ""

#. module: lefilament_cash_flow
#. openerp-web
#: code:addons/lefilament_cash_flow/static/src/xml/cash_flow.xml:10
#, python-format
msgid "Project duration"
msgstr ""

#. module: lefilament_cash_flow
#: model:ir.ui.view,arch_db:lefilament_cash_flow.view_form_lefilament_cashflow_form_inherited
msgid "See the Cash Flow"
msgstr ""

#. module: lefilament_cash_flow
#. openerp-web
#: code:addons/lefilament_cash_flow/static/src/xml/cash_flow.xml:18
#: code:addons/lefilament_cash_flow/static/src/xml/cash_flow.xml:19
#, python-format
msgid "days"
msgstr ""

