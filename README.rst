.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=====================
Le Filament - Projets Cash Flow
=====================

This module depends upon *lefilament_projets* module.

This module provides: 
 - the project cash flow graph

Usage
=====

On each project view you find a new button to *See Cash Flow* that will open the cash flow graph 

Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>
* Remi Cazenave <remi@le-filament.com>
* Juliana Poudou <juliana@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament