# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': 'Le Filament - Cash Flow project',

    'summary': """
        Cash Flow project Le Filament""",

    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'description': """

    Module Cash Flow Project Le Filament

    This module depends upon *lefilament_projets* module.

    This module provides:
    - the project cash flow graph
    """,

    'author': 'LE FILAMENT',
    'category': 'Project',
    'depends': ['lefilament_projets'],
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
        'Rémi Cazenave <remi@le-filament.com>',
        'Juliana Poudou <juliana@le-filament.com>',
    ],
    'website': 'https://le-filament.com',
    'data': [
        'views/assets.xml',
        'views/lefilament_projets_view.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
}
