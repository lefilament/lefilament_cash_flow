# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import datetime
from odoo import models, fields, api


class FilamentProjetCashFlow(models.Model):
    _inherit = 'project.project'

    lf_delay = fields.Integer('Delay facture', compute='_delay')
    lf_total_days = fields.Integer('Project duration', compute='_total_days')
    lf_advance = fields.Float('Advanced cash', compute='_advance')
    lf_balance = fields.Float('Current balance', compute='_balance')

    # Time to first invoice
    @api.one
    def _delay(self):
        account_id = self.analytic_account_id.id
        prospection_tasks = self.env['project.task'].search(
            [('name', '=ilike', 'Prospection'+'%'),
             ('project_id', '=', self.id)]).ids
        if not prospection_tasks:
            self.env.cr.execute("""
                SELECT date
                FROM account_analytic_line
                WHERE account_id=%s AND project_id IS NOT NULL
                ORDER BY date
                ;""", (account_id, ))
        else:
            self.env.cr.execute("""
                SELECT date
                FROM account_analytic_line
                WHERE account_id=%s AND (task_id not IN %s OR task_id IS NULL)
                    AND project_id IS NOT NULL
                ORDER BY date
                ;""", (account_id, tuple(prospection_tasks), ))

        d_cost = self.env.cr.fetchall()

        self.env.cr.execute("""
            SELECT date
            FROM account_analytic_line
            WHERE account_id=%s AND project_id IS NULL AND ref IS NULL
                ORDER BY date LIMIT 1
            ;""", (account_id, ))
        d_invoice = self.env.cr.fetchone()

        if d_cost:
            if d_invoice:
                self.lf_delay = (
                    datetime.strptime(d_invoice[0], "%Y-%m-%d").date()
                    - datetime.strptime(d_cost[0][0], "%Y-%m-%d").date()
                    ).days
            else:
                self.lf_delay = (
                    datetime.strptime(d_cost[-1][0], "%Y-%m-%d").date()
                    - datetime.strptime(d_cost[0][0], "%Y-%m-%d").date()
                    ).days
        else:
            self.lf_delay = 0

    # Total Days
    @api.one
    def _total_days(self):
        account_id = self.analytic_account_id.id
        prospection_tasks = self.env['project.task'].search(
            [('name', '=ilike', 'Prospection'+'%'),
             ('project_id', '=', self.id)]
            ).ids
        if not prospection_tasks:
            self.env.cr.execute("""
                SELECT date(d_last) - date(d_first) AS datediff
                FROM
                    (SELECT date
                     FROM account_analytic_line
                     WHERE account_id=%s AND project_id IS NOT NULL
                     ORDER BY date LIMIT 1) AS d_first,
                    (SELECT date
                     FROM account_analytic_line
                     WHERE account_id=%s AND project_id IS NOT NULL
                     ORDER BY  date DESC LIMIT 1) AS d_last
                ;""", (account_id, account_id, ))
        else:
            self.env.cr.execute(
                """
                SELECT date(d_last) - date(d_first) AS datediff
                FROM
                    (SELECT date FROM account_analytic_line
                     WHERE account_id=%s
                        AND (task_id not IN %s OR task_id IS NULL)
                        AND project_id IS NOT NULL
                     ORDER BY date LIMIT 1) AS d_first,
                    (SELECT date FROM account_analytic_line
                     WHERE account_id=%s
                        AND (task_id not IN %s OR task_id IS NULL)
                        AND project_id IS NOT NULL
                     ORDER BY date DESC LIMIT 1) AS d_last
                ;""",
                (account_id, tuple(prospection_tasks),
                 account_id, tuple(prospection_tasks), )
                )

        lf_total_days = self._cr.fetchone()

        if lf_total_days:
            self.lf_total_days = lf_total_days[0]
        else:
            self.lf_total_days = 0

    # Balance
    @api.one
    def _balance(self):
        account_id = self.analytic_account_id.id
        prospection_tasks = self.env['project.task'].search(
            [('name', '=ilike', 'Prospection'+'%'),
             ('project_id', '=', self.id)]).ids
        if not prospection_tasks:
            self.env.cr.execute("""
                SELECT SUM(amount) AS Total
                FROM account_analytic_line
                WHERE account_id=%s
                ;""", (account_id, ))
            self.lf_balance = self._cr.fetchone()[0]
        else:
            self.env.cr.execute("""
                SELECT SUM(amount) AS Total
                FROM account_analytic_line
                WHERE account_id=%s AND (task_id not IN %s OR task_id IS NULL)
                ;""", (account_id, tuple(prospection_tasks), ))
            self.lf_balance = self._cr.fetchone()[0]

    # Advance cash
    @api.one
    def _advance(self):
        account_id = self.analytic_account_id.id
        prospection_tasks = self.env['project.task'].search(
            [('name', '=ilike', 'Prospection'+'%'),
             ('project_id', '=', self.id)]).ids
        if not prospection_tasks:
            self.env.cr.execute(
                """
                SELECT SUM(amount) AS Total
                FROM account_analytic_line
                WHERE account_id=%s AND date <
                    (CASE WHEN
                        (SELECT date FROM account_analytic_line
                         WHERE account_id=%s
                            AND project_id IS NULL
                            AND ref IS NULL
                         ORDER BY date LIMIT 1) IS NOT NULL
                     THEN
                        (SELECT date FROM account_analytic_line
                         WHERE account_id=%s
                            AND project_id IS NULL
                            AND ref IS NULL
                         ORDER BY date LIMIT 1)
                     ELSE current_date
                     END);
                """,
                (account_id, account_id, account_id, ))
        else:
            self.env.cr.execute(
                """
                SELECT SUM(amount) AS Total
                FROM account_analytic_line
                WHERE account_id=%s AND date <
                    (CASE WHEN
                        (SELECT date FROM account_analytic_line
                         WHERE account_id=%s
                            AND project_id IS NULL
                            AND ref is NULL
                         ORDER BY date LIMIT 1) IS NOT NULL
                     THEN
                        (SELECT date FROM account_analytic_line
                         WHERE account_id=%s
                            AND project_id IS NULL
                            AND ref IS NULL
                         ORDER BY date LIMIT 1)
                     ELSE current_date
                     END)
                     AND (task_id NOT IN %s OR task_id IS NULL);
                """,
                (account_id, account_id, account_id,
                 tuple(prospection_tasks), ))
        self.lf_advance = self._cr.fetchone()[0]

    ####################################################
    #                    Actions                       #
    ####################################################

    def open_cash_flow(self):
        return {
            'type': 'ir.actions.client',
            'name': 'Cash Flow - ' + self.analytic_account_id.name,
            'tag': 'lefilament_cash_flow.cashFlow',
            'target': 'new',
            'params': {
                'account_id': self.analytic_account_id.id,
                'project_id': self.id,
            },
        }

    ####################################################
    #                  Widget Function                 #
    ####################################################

    @api.model
    def cash_flow(self, account_id, project_id):
        account = self.env['account.analytic.account'].search(
            [('id', '=', account_id)])
        project = self.env['project.project'].search([('id', '=', project_id)])

        # Analytic account name
        nom = account.name

        # Cash Flow Request
        prospection_tasks = self.env['project.task'].search(
            [('name', '=ilike', 'Prospection'+'%'),
             ('project_id', '=', project_id)]).ids
        if not prospection_tasks:
            self.env.cr.execute(
                """
                SELECT date,
                    SUM(SUM(amount)) OVER (ORDER BY date) AS Total,
                    SUM(SUM(CASE WHEN project_id IS NOT NULL
                            THEN amount ELSE 0 END))
                        OVER (ORDER BY date) AS Timesheet,
                    SUM(SUM(CASE WHEN project_id IS NULL AND ref IS NOT NULL
                            THEN amount ELSE 0 END))
                        OVER (ORDER BY date) AS Fournisseurs,
                    SUM(SUM(CASE WHEN project_id IS NULL AND ref IS NULL
                            THEN amount ELSE 0 END))
                        OVER (ORDER BY date) AS Clients
                FROM account_analytic_line
                WHERE account_id=%s
                GROUP BY date;
                """, (account_id, ))
        else:
            self.env.cr.execute(
                """
                SELECT date,
                    SUM(SUM(CASE WHEN (task_id NOT IN %s OR task_id IS NULL)
                            THEN amount ELSE 0 END))
                        OVER (ORDER BY date) AS Total,
                    SUM(SUM(CASE WHEN project_id IS NOT NULL
                                    AND (task_id not in %s or task_id IS NULL)
                            THEN amount ELSE 0 END))
                        OVER (ORDER BY date) AS Timesheet,
                    SUM(SUM(CASE WHEN project_id IS NULL AND ref IS NOT NULL
                            THEN amount ELSE 0 END))
                        OVER (ORDER BY date) AS Fournisseurs,
                    SUM(SUM(CASE WHEN project_id IS NULL AND ref IS NULL
                            THEN amount ELSE 0 END))
                        OVER (ORDER BY date) AS Clients
                FROM account_analytic_line
                WHERE account_id=%s
                GROUP BY date;
                """,
                (tuple(prospection_tasks), tuple(prospection_tasks),
                 account_id, ))
        cash_flow = self._cr.dictfetchall()

        # Time to first invoice
        delay = project.lf_delay

        # Total Days
        total_days = project.lf_total_days

        # Balance
        balance = project.lf_balance

        # Advance cash
        advance = project.lf_advance

        return {
            'nom': nom,
            'cash_flow': cash_flow,
            'delay': delay,
            'balance': balance,
            'advance': advance,
            'total_days': total_days,
            }
