// © 2017 Le Filament (<http://www.le-filament.com>)
// License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

odoo.define('lefilament_cash_flow.cashFlow', function(require) {
  'use strict';

  var core = require('web.core');
  var data = require('web.data');
  var Widget = require('web.Widget');
  var Model = require('web.Model');
  var form_common = require('web.form_common');

  var _t = core._t;
  var QWeb = core.qweb;

  var CashFlow = Widget.extend({
    template: "CashFlow",

    init: function(parent, action) {
        this._super(parent, action);
        this.account_id = action.params.account_id;
        this.project_id = action.params.project_id;
    },

    willStart: function() {
          var deferred = new jQuery.Deferred();
          var self = this;
          self.cash_datas = {};
          
          var dash_model = new Model('project.project');
          dash_model.call('cash_flow', [self.account_id, self.project_id])
              .then(function(results) {
                  self.cash_datas = results.cash_flow;
                  self.project = results.nom;
                  self.delay = results.delay;
                  self.total_days = results.total_days;
                  self.balance = results.balance;
                  self.advance = results.advance;
                  deferred.resolve();
              });
            
          return jQuery.when(this._super.apply(this, arguments),deferred);
    },
    
    start: function() {
          var sup = this._super();
          
          this.display_chart();
          return sup;
    },

    display_chart: function() {
          var self = this;

          this.ctx = this.$el.find('#cash_flow')[0].getContext('2d');

          var labels = [];

          var total = [];
          var heures = [];
          var fournisseurs = [];
          var clients = [];

          _.each( self.cash_datas, function(value, key) {
            labels.push(moment(value.date));
            total.push(value.total);
            heures.push(value.timesheet);
            fournisseurs.push(value.fournisseurs);
            clients.push(value.clients);
          });
              
            
        var datasets = [
          { label: 'Total', data: total, backgroundColor: 'transparent',borderColor: '#5E8ED5', },
          { label: 'Timesheet', data: heures, backgroundColor: 'transparent',borderColor: '#FFDFA9', borderWidth: 1.5, radius: 0,  },
          { label: 'Client', data: clients, backgroundColor: 'transparent',borderColor: '#51d2b7', borderWidth: 1.5, radius: 0, },
          { label: 'Fournisseur', data: fournisseurs, backgroundColor: 'transparent',borderColor: '#FCA7B3', borderWidth: 1.5, radius: 0, },
        ];

        var options = {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true,
                      }
                  }],
                  xAxes: [{
                    type: 'time',
                    time: {
                      displayFormats: {
                        'millisecond': 'DD MMM',
                        'second': 'DD MMM',
                        'minute': 'DD MMM',
                        'hour': 'DD MMM',
                        'day': 'DD MMM',
                        'week': 'DD MMM',
                        'month': 'DD MMM',
                        'quarter': 'DD MMM',
                        'year': 'DD MMM',
                      },
                    },
                   }],
              },
              tooltips: {
              backgroundColor: 'rgba(255,255,255,0.8)',
              titleFontStyle: 'normal',
              titleFontColor: '#999',
              bodyFontColor: '#777',
              callbacks: {
                      label: function(tooltipItems, data) { 
                          return ' ' + (tooltipItems.yLabel).toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €';
                      },
                      title: function(tooltipItems, data) { 
                          return ' ' + (tooltipItems[0].xLabel._d).toLocaleString('fr', {day: 'numeric', month:'short', year:'2-digit'});
                      }
                  }
            },
            responsive: true,
          }

        this.targetData = {
          labels : labels,
          datasets : datasets
        };

        var myChart = new Chart(this.ctx, { type: 'line', data: this.targetData, options: options } );
    },

    render_monetary: function(value) {
          value = value.toLocaleString('fr', { maximumFractionDigits: 0 }) + ' €';
          return value;
    },

  });

    core.action_registry.add('lefilament_cash_flow.cashFlow', CashFlow);

});
